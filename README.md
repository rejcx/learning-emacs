# Learning Emacs

C-x means "CTRL"
M-x means "ALT"

## Program options

| Exit | C-x C-c |

## Normal text editing commands

| Undo		| C-x u |
| Undo command 	| C-/ |
| Redo 		| |
| Select all | C-x h |
| Cut | C-w |
| Copy | M-w |
| Paste | C-y |
| Save 	      | C-x C-s |
| Load (Find) | C-x C-f |
| Go to end of line | C-e |
| Go to beginning of line | C-a |

## Replace the right side of the keyboard & the mouse

You can use the arrow keys to move around text,
as well as home/end and page up/page down, but you might
not want to move your hands from the setandard home row, slowing down.

### Text navigation

| Action    	      	    	     	 | From home row | From keyboard right side |
| ------   	     	   		 | ------------- | ------------------------ |
| Move 1 character left (backward)  	 | C-b 		 | Left arrow		    |
| Move 1 character right (forward) 	 | C-f 		 | Right arrow		    |
| Move 1 line up (previous) 		 | C-p 		 | Up arrow		    |
| Move 1 line down (next) 		 | C-n 		 | Down arrow		    |
| Move 1 word left 			 | M-f 		 | CTRL + Left arrow	    |
| Move 1 word right 			 | M-b 		 | CTRL + Right arrow	    |
| Move 1 sentence left 			 | M-a 		 |			    |
| Move 1 sentence right 		 | M-e 		 |			    |
| Move to beginning of line 		 | C-a 		 | Home			    |
| Move to end of line  			 | C-e 		 | End			    |	
| Move down 1 page     	 		 | C-v		 | Page down		    |
| Move up 1 page       			 | M-v 		 | Page up		    |

### Other side keys

| Action	   | From home row | From keyboard right side |
| ---------------- | ------------- | ------------------------ |
| Delete character | C-d	   | Delete		      |
| Delete word	   | M-d	   | M-Delete		      |

### Mouse usage

| Action	 | From home row	| With mouse | 
| -------------- | -------------------- | ---------- |
| Highlight text | C-Space, move, C-w	| Click & drag with mouse, C-w |

## Sorting text

https://www.gnu.org/software/emacs/manual/html_node/emacs/Sorting.html

| Sort lines | M-x sort-lines |
| Reverse sort region | M-x reverse-region |

## Useful items for programming

### Appending the same thing to the end of each line

| 1. | C-x ( | Start a macro |
| 2. | C-e | Go to end of line |
| 3. | text | Text to append |
| 4. | C-n | Go to next line |
| 5. | C-x ) | End of macro |
| 6. | C-x e | Execute macro |

Keep hitting ```e``` to continue executing it.

Source: https://stackoverflow.com/questions/4870704/appending-characters-to-the-end-of-each-line-in-emacs

## New commands

(Commands I don't know in other text editors)

| C-k | Kill the rest of a line / bring the next line up to this spot. |
| C-y | Yank the last killed text (Like C-k, C-u in Nano)   	       |
